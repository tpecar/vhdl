------------------------------------------------------------------------------
-- Create Date:    15:25:54 02/18/2017 
-- Module Name:    VGA_timing_ctrl - VGA_timing_ctrl_impl 
-- Za tvorbo sinhronizacijskih signalov za monitor.
------------------------------------------------------------------------------
library IEEE;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
------------------------------------------------------------------------------
-- timing konstante za posamezne resolucije, na podlagi katerih izracunamo
-- meje stevcev za prozenje hsync, vsync, hvidon, vvidon signalov
--
-- Format zapisa ter konstante za resolucije so bile pridobljene iz
-- DMT, ver 1.0, rev 11
-- DMT, ver 1.0, rev 13 za 720p, 1080p resolucije
-- slednji je (uradno) dostopen na
-- https://app.box.com/s/vcocw3z73ta09txiskj7cnk6289j356b/1/11133572986
--
package DMT_VGA_timing is
  -- definicija struktur --
  type VGA_Scan_type is (NONINTERLACED, INTERLACED);
  type VGA_Sync_Polarity is (SYN_NEGATIVE, SYN_POSITIVE);

  type VGA_timing is record
    Timing_Name       : string (1 to 31);
    Hor_Pixels        : integer;              -- Pixels
    Ver_Pixels        : integer;              -- Lines
    Hor_Frequency     : real;                 -- kHz
    Ver_Frequency     : real;                 -- Hz
    Pixel_Clock       : real;                 -- MHz
    Character_Width   : integer;              -- Pixels
    Scan_Type         : VGA_Scan_type;        -- H Phase %
    Hor_Sync_Polarity : VGA_Sync_Polarity;    -- HBlank %
    Ver_Sync_Polarity : VGA_Sync_Polarity;    -- VBlank %
    Hor_Total_Time    : real;                 -- (usec)
    Hor_Addr_Time     : real;                 -- (usec)
    Hor_Blank_Start   : real;                 -- (usec)
    Hor_Blank_Time    : real;                 -- (usec)
    Hor_Sync_Start    : real;                 -- (usec)
    H_Right_Border    : real;                 -- (usec)
    H_Front_Porch     : real;                 -- (usec)
    Hor_Sync_Time     : real;                 -- (usec)
    H_Back_Porch      : real;                 -- (usec)
    H_Left_Border     : real;                 -- (usec)
    Ver_Total_Time    : real;                 -- (msec)
    Ver_Addr_Time     : real;                 -- (msec)
    Ver_Blank_Start   : real;                 -- (msec)
    Ver_Blank_Time    : real;                 -- (msec)
    Ver_Sync_Start    : real;                 -- (msec)
    V_Bottom_Border   : real;                 -- (msec)
    V_Front_Porch     : real;                 -- (msec)
    Ver_Sync_Time     : real;                 -- (msec)
    V_Back_Porch      : real;                 -- (msec)
    V_Top_Border      : real;                 -- (msec)
  end record VGA_timing;
  ----------------------------------------------------------------------------
  -- definicija timing konstant --
  -- potrebno je raziskati lepsi nacin za padding nizov, kot pa rocno stetje
  -- potrebnih presledkov
  
  -- standardna tekstovna VGA resolucija             
  constant VGA_720x480_85Hz : VGA_timing :=
  (
    Timing_Name       => "720 x 400 @ 85Hz" & "               ",
    Hor_Pixels        => 720,                 -- Pixels
    Ver_Pixels        => 400,                 -- Lines
    Hor_Frequency     => 37.927,              -- kHz = 26.4 usec / line
    Ver_Frequency     => 85.039,              -- Hz = 11.8 msec / frame
    Pixel_Clock       => 35.500,              -- MHz = 28.2 nsec ± 0.5% 
    Character_Width   => 9,                   -- Pixels = 253.5 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 3.8 %
    Hor_Sync_Polarity => SYN_NEGATIVE,        -- HBlank = 23.1% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 10.3% of VTotal
    Hor_Total_Time    => 26.366,              -- (usec) = 104 chars = 936 Pixels 
    Hor_Addr_Time     => 20.282,              -- (usec) = 80 chars = 720 Pixels 
    Hor_Blank_Start   => 20.282,              -- (usec) = 80 chars = 720 Pixels
    Hor_Blank_Time    => 6.085,               -- (usec) = 24 chars = 216 Pixels
    Hor_Sync_Start    => 21.296,              -- (usec) = 84 chars = 756 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 1.014,               -- (usec) = 4 chars = 36 Pixels 
    Hor_Sync_Time     => 2.028,               -- (usec) = 8 chars = 72 Pixels
    H_Back_Porch      => 3.042,               -- (usec) = 12 chars = 108 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels 
    Ver_Total_Time    => 11.759,              -- (msec) = 446 lines HT - (1.06xHA)
    Ver_Addr_Time     => 10.546,              -- (msec) = 400 lines = 4.87 
    Ver_Blank_Start   => 10.546,              -- (msec) = 400 lines 
    Ver_Blank_Time    => 1.213,               -- (msec) = 46 lines
    Ver_Sync_Start    => 10.573,              -- (msec) = 401 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.026,               -- (msec) = 1 lines
    Ver_Sync_Time     => 0.079,               -- (msec) = 3 lines
    V_Back_Porch      => 1.107,               -- (msec) = 42 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  -- standardna graficna VGA resolucija
  constant VGA_640x480_60Hz : VGA_timing :=
  (
    Timing_Name       => "640 x 480 @ 60Hz" & "               ",
    Hor_Pixels        => 640,                 -- Pixels
    Ver_Pixels        => 480,                 -- Lines
    Hor_Frequency     => 31.469,              -- kHz = 31.8 usec / line
    Ver_Frequency     => 59.940,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 25.175,              -- MHz = 39.7 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 317.8 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 2.0 %
    Hor_Sync_Polarity => SYN_NEGATIVE,        -- HBlank = 18.0% of HTotal
    Ver_Sync_Polarity => SYN_NEGATIVE,        -- VBlank = 5.5% of VTotal
    Hor_Total_Time    => 31.778,              -- (usec) = 100 chars = 800 Pixels
    Hor_Addr_Time     => 25.422,              -- (usec) = 80 chars = 640 Pixels
    Hor_Blank_Start   => 25.740,              -- (usec) = 81 chars = 648 Pixels
    Hor_Blank_Time    => 5.720,               -- (usec) = 18 chars = 144 Pixels
    Hor_Sync_Start    => 26.058,              -- (usec) = 82 chars = 656 Pixels
    H_Right_Border    => 0.318,               -- (usec) = 1 chars = 8 Pixels
    H_Front_Porch     => 0.318,               -- (usec) = 1 chars = 8 Pixels
    Hor_Sync_Time     => 3.813,               -- (usec) = 12 chars = 96 Pixels
    H_Back_Porch      => 1.589,               -- (usec) = 5 chars = 40 Pixels
    H_Left_Border     => 0.318,               -- (usec) = 1 chars = 8 Pixels
    Ver_Total_Time    => 16.683,              -- (msec) = 525 lines HT - (1.06xHA)
    Ver_Addr_Time     => 15.253,              -- (msec) = 480 lines = 4.83
    Ver_Blank_Start   => 15.507,              -- (msec) = 488 lines
    Ver_Blank_Time    => 0.922,               -- (msec) = 29 lines
    Ver_Sync_Start    => 15.571,              -- (msec) = 490 lines
    V_Bottom_Border   => 0.254,               -- (msec) = 8 lines
    V_Front_Porch     => 0.064,               -- (msec) = 2 lines
    Ver_Sync_Time     => 0.064,               -- (msec) = 2 lines
    V_Back_Porch      => 0.794,               -- (msec) = 25 lines
    V_Top_Border      => 0.254                -- (msec) = 8 lines
  );
  constant VGA_800x600_60Hz : VGA_timing :=
  (
    Timing_Name       => "800 x 600 @ 60Hz" & "               ",
    Hor_Pixels        => 800,                 -- Pixels 
    Ver_Pixels        => 600,                 -- Lines
    Hor_Frequency     => 37.879,              -- kHz = 26.4 usec / line
    Ver_Frequency     => 60.317,              -- Hz = 16.6 msec / frame
    Pixel_Clock       => 40.000,              -- MHz = 25.0 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 200.0 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 2.3 %
    Hor_Sync_Polarity => SYN_POSITIVE,        -- HBlank = 24.2% of HTotal 
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 4.5% of VTotal
    Hor_Total_Time    => 26.400,              -- (usec) = 132 chars = 1056 Pixels
    Hor_Addr_Time     => 20.000,              -- (usec) = 100 chars = 800 Pixels
    Hor_Blank_Start   => 20.000,              -- (usec) = 100 chars = 800 Pixels
    Hor_Blank_Time    => 6.400,               -- (usec) = 32 chars = 256 Pixels
    Hor_Sync_Start    => 21.000,              -- (usec) = 105 chars = 840 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 1.000,               -- (usec) = 5 chars = 40 Pixels
    Hor_Sync_Time     => 3.200,               -- (usec) = 16 chars = 128 Pixels
    H_Back_Porch      => 2.200,               -- (usec) = 11 chars = 88 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.579,              -- (msec) = 628 lines HT - (1.06xHA)
    Ver_Addr_Time     => 15.840,              -- (msec) = 600 lines = 5.2
    Ver_Blank_Start   => 15.840,              -- (msec) = 600 lines
    Ver_Blank_Time    => 0.739,               -- (msec) = 28 lines
    Ver_Sync_Start    => 15.866,              -- (msec) = 601 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.026,               -- (msec) = 1 lines
    Ver_Sync_Time     => 0.106,               -- (msec) = 4 lines
    V_Back_Porch      => 0.607,               -- (msec) = 23 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  constant VGA_1024x768_60Hz : VGA_timing :=
  (
    Timing_Name       => "1024 x 768 @ 60Hz" & "              ",
    Hor_Pixels        => 1024,                -- Pixels
    Ver_Pixels        => 768,                 -- Lines
    Hor_Frequency     => 48.363,              -- kHz = 20.7 usec / line
    Ver_Frequency     => 60.004,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 65.000,              -- MHz = 15.4 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 123.1 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 5.1 %
    Hor_Sync_Polarity => SYN_NEGATIVE,        -- HBlank = 23.8% of HTotal
    Ver_Sync_Polarity => SYN_NEGATIVE,        -- VBlank = 4.7% of VTotal
    Hor_Total_Time    => 20.677,              -- (usec) = 168 chars = 1344 Pixels
    Hor_Addr_Time     => 15.754,              -- (usec) = 128 chars = 1024 Pixels
    Hor_Blank_Start   => 15.754,              -- (usec) = 128 chars = 1024 Pixels
    Hor_Blank_Time    => 4.923,               -- (usec) = 40 chars = 320 Pixels
    Hor_Sync_Start    => 16.123,              -- (usec) = 131 chars = 1048 Pixels 
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 0.369,               -- (usec) = 3 chars = 24 Pixels
    Hor_Sync_Time     => 2.092,               -- (usec) = 17 chars = 136 Pixels 
    H_Back_Porch      => 2.462,               -- (usec) = 20 chars = 160 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.666,              -- (msec) = 806 lines HT - (1.06xHA)
    Ver_Addr_Time     => 15.880,              -- (msec) = 768 lines = 3.98
    Ver_Blank_Start   => 15.880,              -- (msec) = 768 lines
    Ver_Blank_Time    => 0.786,               -- (msec) = 38 lines
    Ver_Sync_Start    => 15.942,              -- (msec) = 771 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.062,               -- (msec) = 3 lines
    Ver_Sync_Time     => 0.124,               -- (msec) = 6 lines
    V_Back_Porch      => 0.600,               -- (msec) = 29 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  -- 720p (HD) resolucija
  constant VGA_1280x720_60Hz : VGA_timing :=
  (
    Timing_Name       => "1280 x 720 @ 60Hz" & "              ",
    Hor_Pixels        => 1280,                -- Pixels
    Ver_Pixels        => 720,                 -- Lines
    Hor_Frequency     => 45.000,              -- KHz = 22.2 usec / line
    Ver_Frequency     => 60.000,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 74.250,              -- MHz = 13.5 nsec ± 0.5% 
    Character_Width   => 1,                   -- Pixels = 13.5 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 3.3 %
    Hor_Sync_Polarity => SYN_POSITIVE,        -- HBlank = 22.4% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 4.0% of VTotal
    Hor_Total_Time    => 22.222,              -- (usec) = 1650 chars = 1650 Pixels 
    Hor_Addr_Time     => 17.239,              -- (usec) = 1280 chars = 1280 Pixels
    Hor_Blank_Start   => 17.239,              -- (usec) = 1280 chars = 1280 Pixels
    Hor_Blank_Time    => 4.983,               -- (usec) = 370 chars = 370 Pixels
    Hor_Sync_Start    => 18.721,              -- (usec) = 1390 chars = 1390 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 1.481,               -- (usec) = 110 chars = 110 Pixels
    Hor_Sync_Time     => 0.539,               -- (usec) = 40 chars = 40 Pixels
    H_Back_Porch      => 2.963,               -- (usec) = 220 chars = 220 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.667,              -- (msec) = 750 lines HT - (1.06xHA)
    Ver_Addr_Time     => 16.000,              -- (msec) = 720 lines = 3.95
    Ver_Blank_Start   => 16.000,              -- (msec) = 720 lines
    Ver_Blank_Time    => 0.667,               -- (msec) = 30 lines 
    Ver_Sync_Start    => 16.111,              -- (msec) = 725 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.111,               -- (msec) = 5 lines 
    Ver_Sync_Time     => 0.111,               -- (msec) = 5 lines
    V_Back_Porch      => 0.444,               -- (msec) = 20 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  constant VGA_1280x800_60Hz : VGA_timing :=
  (
    Timing_Name       => "1280 x 800 @ 60Hz" & "              ",
    Hor_Pixels        => 1280,                -- Pixels
    Ver_Pixels        => 800,                 -- Lines
    Hor_Frequency     => 49.702,              -- kHz = 20.1 usec / line
    Ver_Frequency     => 59.810,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 83.500,              -- MHz = 12.0 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 95.8 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 3.8 %
    Hor_Sync_Polarity => SYN_NEGATIVE,        -- HBlank = 23.8% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 3.7% of VTotal
    Hor_Total_Time    => 20.120,              -- (usec) = 210 chars = 1680 Pixels
    Hor_Addr_Time     => 15.329,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Start   => 15.329,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Time    => 4.790,               -- (usec) = 50 chars = 400 Pixels
    Hor_Sync_Start    => 16.192,              -- (usec) = 169 chars = 1352 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 0.862,               -- (usec) = 9 chars = 72 Pixels
    Hor_Sync_Time     => 1.533,               -- (usec) = 16 chars = 128 Pixels
    H_Back_Porch      => 2.395,               -- (usec) = 25 chars = 200 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.720,              -- (msec) = 831 lines HT - (1.06xHA)
    Ver_Addr_Time     => 16.096,              -- (msec) = 800 lines = 3.87
    Ver_Blank_Start   => 16.096,              -- (msec) = 800 lines
    Ver_Blank_Time    => 0.624,               -- (msec) = 31 lines
    Ver_Sync_Start    => 16.156,              -- (msec) = 803 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.060,               -- (msec) = 3 lines
    Ver_Sync_Time     => 0.121,               -- (msec) = 6 lines
    V_Back_Porch      => 0.443,               -- (msec) = 22 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  constant VGA_1280x960_60Hz : VGA_timing :=
  (
    Timing_Name       => "1280 x 960 @ 60Hz" & "              ",
    Hor_Pixels        => 1280,                -- Pixels
    Ver_Pixels        => 960,                 -- Lines
    Hor_Frequency     => 60.000,              -- kHz = 16.7 usec / line
    Ver_Frequency     => 60.000,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 108.000,             -- MHz = 9.3 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 74.1 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 6.0 %
    Hor_Sync_Polarity => SYN_POSITIVE,        -- HBlank = 28.9% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 4.0% of VTotal
    Hor_Total_Time    => 16.667,              -- (usec) = 225 chars = 1800 Pixels
    Hor_Addr_Time     => 11.852,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Start   => 11.852,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Time    => 4.815,               -- (usec) = 65 chars = 520 Pixels
    Hor_Sync_Start    => 12.741,              -- (usec) = 172 chars = 1376 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 0.889,               -- (usec) = 12 chars = 96 Pixels
    Hor_Sync_Time     => 1.037,               -- (usec) = 14 chars = 112 Pixels
    H_Back_Porch      => 2.889,               -- (usec) = 39 chars = 312 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.667,              -- (msec) = 1000 lines HT - (1.06xHA)
    Ver_Addr_Time     => 16.000,              -- (msec) = 960 lines = 4.1
    Ver_Blank_Start   => 16.000,              -- (msec) = 960 lines
    Ver_Blank_Time    => 0.667,               -- (msec) = 40 lines
    Ver_Sync_Start    => 16.017,              -- (msec) = 961 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.017,               -- (msec) = 1 lines
    Ver_Sync_Time     => 0.050,               -- (msec) = 3 lines
    V_Back_Porch      => 0.600,               -- (msec) = 36 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  constant VGA_1280x1024_60Hz : VGA_timing :=
  (
    Timing_Name       => "1280 x 1024 @ 60Hz" & "             ",
    Hor_Pixels        => 1280,                -- Pixels
    Ver_Pixels        => 1024,                -- Lines
    Hor_Frequency     => 63.981,              -- kHz = 15.6 usec / line
    Ver_Frequency     => 60.020,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 108.000,             -- MHz = 9.3 nsec ± 0.5%
    Character_Width   => 8,                   -- Pixels = 74.1 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 5.9 %
    Hor_Sync_Polarity => SYN_POSITIVE,        -- HBlank = 24.2% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 3.9% of VTotal
    Hor_Total_Time    => 15.630,              -- (usec) = 211 chars = 1688 Pixels
    Hor_Addr_Time     => 11.852,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Start   => 11.852,              -- (usec) = 160 chars = 1280 Pixels
    Hor_Blank_Time    => 3.778,               -- (usec) = 51 chars = 408 Pixels
    Hor_Sync_Start    => 12.296,              -- (usec) = 166 chars = 1328 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 0.444,               -- (usec) = 6 chars = 48 Pixels
    Hor_Sync_Time     => 1.037,               -- (usec) = 14 chars = 112 Pixels
    H_Back_Porch      => 2.296,               -- (usec) = 31 chars = 248 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.661,              -- (msec) = 1066 lines HT - (1.06xHA)
    Ver_Addr_Time     => 16.005,              -- (msec) = 1024 lines = 3.07
    Ver_Blank_Start   => 16.005,              -- (msec) = 1024 lines
    Ver_Blank_Time    => 0.656,               -- (msec) = 42 lines
    Ver_Sync_Start    => 16.020,              -- (msec) = 1025 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.016,               -- (msec) = 1 lines
    Ver_Sync_Time     => 0.047,               -- (msec) = 3 lines
    V_Back_Porch      => 0.594,               -- (msec) = 38 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  -- 1080p (Full HD) resolucija
  constant VGA_1920x1080_60Hz : VGA_timing :=
  (
    Timing_Name       => "1920 x 1080 @ 60Hz" & "             ",
    Hor_Pixels        => 1920,                -- Pixels
    Ver_Pixels        => 1080,                -- Lines
    Hor_Frequency     => 67.500,              -- kHz = 14.8 usec / line
    Ver_Frequency     => 60.000,              -- Hz = 16.7 msec / frame
    Pixel_Clock       => 148.500,             -- MHz = 6.7 nsec ± 0.5%
    Character_Width   => 4,                   -- Pixels = 26.9 nsec
    Scan_Type         => NONINTERLACED,       -- H Phase = 1.4 %
    Hor_Sync_Polarity => SYN_POSITIVE,        -- HBlank = 12.7% of HTotal
    Ver_Sync_Polarity => SYN_POSITIVE,        -- VBlank = 4.0% of VTotal
    Hor_Total_Time    => 14.815,              -- (usec) = 550 chars = 2200 Pixels
    Hor_Addr_Time     => 12.929,              -- (usec) = 480 chars = 1920 Pixels
    Hor_Blank_Start   => 12.929,              -- (usec) = 480 chars = 1920 Pixels
    Hor_Blank_Time    => 1.886,               -- (usec) = 70 chars = 280 Pixels
    Hor_Sync_Start    => 13.522,              -- (usec) = 502 chars = 2008 Pixels
    H_Right_Border    => 0.000,               -- (usec) = 0 chars = 0 Pixels
    H_Front_Porch     => 0.593,               -- (usec) = 22 chars = 88 Pixels
    Hor_Sync_Time     => 0.296,               -- (usec) = 11 chars = 44 Pixels
    H_Back_Porch      => 0.997,               -- (usec) = 37 chars = 148 Pixels
    H_Left_Border     => 0.000,               -- (usec) = 0 chars = 0 Pixels
    Ver_Total_Time    => 16.667,              -- (msec) = 1125 lines HT - (1.06xHA)
    Ver_Addr_Time     => 116.000,             -- (msec) = 1080 lines = 1.11
    Ver_Blank_Start   => 16.000,              -- (msec) = 1080 lines
    Ver_Blank_Time    => 0.667,               -- (msec) = 45 lines
    Ver_Sync_Start    => 16.059,              -- (msec) = 1084 lines
    V_Bottom_Border   => 0.000,               -- (msec) = 0 lines
    V_Front_Porch     => 0.059,               -- (msec) = 4 lines
    Ver_Sync_Time     => 0.074,               -- (msec) = 5 lines
    V_Back_Porch      => 0.533,               -- (msec) = 36 lines
    V_Top_Border      => 0.000                -- (msec) = 0 lines
  );
  ----------------------------------------------------------------------------
  -- funkcije/strukture za preracun timingov v cikle stevca, glede na njegovo
  -- frekvenco (v MHz)
  type VGA_timing_ticks is record
    Pixel_Count     : integer;
    Sync_Polarity   : VGA_Sync_Polarity;
    Total_Time      : integer;
    Addr_Time       : integer;
    Blank_Start     : integer;
    Blank_Time      : integer;
    Sync_Start      : integer;
    Last_Border     : integer; -- zadnji rob (tj. desni ce horiz., spodnji, ce vert.)
    Front_Porch     : integer;
    Sync_Time       : integer;
    Back_Porch      : integer;
    First_Border    : integer; -- prvi rob (tj. levi ce horiz., zgornji, ce vert.)
  end record VGA_timing_ticks;
  
  -- deklaracija vseh funkcij (za opis glej telo paketa)
  function getRequiredBits(maxIndex : integer) return integer;
  function VGA_hsync_ticks(timing : VGA_timing; base_freq : real) return VGA_timing_ticks;
  function VGA_vsync_ticks(timing : VGA_timing; base_freq : real) return VGA_timing_ticks;
end package DMT_VGA_timing;

package body DMT_VGA_timing is
  -- [ENT] izracuna minimalno potrebno stevilo bitov, ki ga mora vektor hraniti za podan najvecji indeks
  function getRequiredBits(maxIndex : integer) return integer is
  begin
    -- ce je maksimalen indeks 0 (1 vrednost), 1 (2 vrednosti), potrebujemo vsaj 1 bit za naslov
    if(maxIndex < 2) then
      return 1;
    end if;
    
    return integer(ceil(log2(real(maxIndex))));
  end function getRequiredBits;
  
  -- trenutna ideja je, da uporabimo DCM (pri Spartan3E na Nexys2),
  -- oz. MMCM (pri Artix 7 na Nexys4)
  -- nekoliko nerodno je, da je le MMCM programljiv on-the-fly, torej smo
  -- z DCM omejeni na eno specificno frekvenco
  --
  -- ce delamo v sklopu omejitev DCMa, potem moramo
  --  1. imeti master clock, ki je (vsaj priblizni) veckratnik pixel clock
  --     frekvenc, da ga lahko delimo (razen v primeru 1080p, 720p tega
  --     prakticno ni mogoce narediti)
  --     Morda bi z zelo grobo oceno za
  --        - VGA_720x480_85Hz (35 Mhz) ter 640 x 480 @ 60Hz (25 Mhz)
  --          prisel cez z uporabo 175 Mhz (x5, x7)
  --        - VGA_640x480_60Hz (25 Mhz) ter VGA_800x600_60Hz (40 Mhz)
  --          prisel cez z uporabo 200 Mhz (x8, x4)
  --     tezava pa se pojavi da __vsi__ master clock-i padejo v podrocje
  --     med LF, HF rangom (90 - 220 Mhz), ki jih DCM ni zmozen generirati
  --     (vsaj glede na DS312, str 143)
  --  2. imeti posebej DCM za vsako izmed frekvenc - to sicer ni najbolj
  --     optimalna resitev, saj jih imamo (v primeru XC3S500E) samo 4 na voljo
  --
  --  In ce to ze nekako resimo, ostaja tu neresen problem sinhronizacije
  --  med preostalim delom FPGA
  --    - ali naj preprosto spremenimo uro celotnega vezja (raje ne)
  --    - ali pa ju povezujejo registri, pri cemer se na obeh koncih
  --      uporablja state machine za koordinacijo prenosa - to je mozno,
  --      a s tem nismo resili problema metastabilnih stanj, ki se lahko
  --      pojavi zaradi nesinhroniziranih ur
  --
  --   najenostavnejsi nacin implementacije zgornjih idej bi najbrz bil
  --   "double buffering" (oz. FIFO) pristop, kjer
  --    1. posiljatelj polne register in po uspesnem zapisu da ready=1 signal (caka na ack=1)
  --    2. ko je bralec pripravljen ter je ready=1, ta zamenja bralno/pisalna registra (on vodi mux), ter da ack=1
  --    3. ko posiljatelj prejme ack, da ready=0 ter pricne s pisanjem
  --        (mux mu je bil nastavljen)
  --   mozna stanja so s tem
  --    rdy ack
  --      0   0   posiljatelj polne X, bralec bere Y
  --      0   1   (vmesno stanje vodila) posiljatelj je registriral ack=1 ter ze zacel s pisanjem,
  --              bralec se ni registriral ready=0, ga bo sele v naslednjem samplanju
  --      1   0   (vmesno stanje vodila) posiljatelj je prenehal s pisanjem v X, bralec tega se ni registriral
  --              ali pa ta se ni pripravljen
  --              (ce bomo bufferali vrstico, bo ta predvidoma pripravljen sele znotraj hblank obmocja, torej
  --               borderji ter porch)
  --      1   1   (vmesno stanje vodila) bralec je zamenjal X in Y ter nakazal zamenjavo z ack=1,
  --              posiljatelj tega se ni registriral in je v stanju cakanja
  --    stanja se spreminjajo na sledec nacin: 00, 10, 11, 01, 00, ... torej gre za gray kodo, kar na splosno v redu
  
  
  -- doloci meje stevcev za horizontalno sinhronizacijo (hsync) na podlagi ure stevcev
  -- (ki bodo predvidoma tvorjena preko DCM, MMCM)
  --
  -- pri tem se cas meri v mikrosekundah (usec), frekvenca stevca je podana v MHz
  -- 1 Mhz = 10^-6, 1 cikel je 1 usec
  function VGA_hsync_ticks(timing : VGA_timing; base_freq : real) return VGA_timing_ticks is
    variable calcTicks        : VGA_timing_ticks;
    variable checkSum         : integer;
  begin
    calcTicks.Pixel_Count     := timing.Hor_Pixels;
    calcTicks.Sync_Polarity   := timing.Hor_Sync_Polarity;
    calcTicks.Total_Time      := integer(round(timing.Hor_Total_Time   * base_freq));
    calcTicks.Addr_Time       := integer(round(timing.Hor_Addr_Time    * base_freq));
    calcTicks.Blank_Start     := integer(round(timing.Hor_Blank_Start  * base_freq));
    calcTicks.Blank_Time      := integer(round(timing.Hor_Blank_Time   * base_freq));
    calcTicks.Sync_Start      := integer(round(timing.Hor_Sync_Start   * base_freq));
    calcTicks.Last_Border     := integer(round(timing.H_Right_Border   * base_freq));
    calcTicks.Front_Porch     := integer(round(timing.H_Front_Porch    * base_freq));
    calcTicks.Sync_Time       := integer(round(timing.Hor_Sync_Time    * base_freq));
    calcTicks.Back_Porch      := integer(round(timing.H_Back_Porch     * base_freq));
    calcTicks.First_Border    := integer(round(timing.H_Left_Border    * base_freq));
    
    checkSum := calcTicks.Back_Porch +
                calcTicks.First_Border +
                calcTicks.Addr_Time +
                calcTicks.Last_Border +
                calcTicks.Front_Porch +
                calcTicks.Sync_Time;
    if checkSum /= calcTicks.Total_Time then
      report "WARNING: Standard specified horizontal total time [" & integer'image(calcTicks.Total_Time) & "]" &
             " differs from sum [" & integer'image(checkSum) & "]! Will use checksum!";
      calcTicks.Total_Time := checkSum;
    end if;
    return calcTicks;
  end function VGA_hsync_ticks;
  
  -- doloci meje stevcev za vertikalno sinhronizacijo (vsync)
  -- pri tem se cas meri v milisekundah (msec), torej 10^3 usec, v katerem je podana
  --    frekvenca stevca
  -- in zaradi same zasnove stevca vsync-a (ta se namrec prozi le ko hsync gre skozi
  -- celotno vrstico), se vsi casi izracunajo glede na cas vrstice hsync-a
  -- zaokrozujemo navzgor
  function VGA_vsync_ticks(timing : VGA_timing; base_freq : real) return VGA_timing_ticks is
    variable calcTicks      : VGA_timing_ticks;
    variable ticksPerLine   : real := 1.0e3 / timing.Hor_Total_Time;
    variable checkSum       : integer;
  begin
    calcTicks.Pixel_Count   := timing.Ver_Pixels;
    calcTicks.Sync_Polarity := timing.Ver_Sync_Polarity;
    calcTicks.Total_Time    := integer(round(timing.Ver_Total_Time   * ticksPerLine));
    calcTicks.Addr_Time     := integer(round(timing.Ver_Addr_Time    * ticksPerLine));
    calcTicks.Blank_Start   := integer(round(timing.Ver_Blank_Start  * ticksPerLine));
    calcTicks.Blank_Time    := integer(round(timing.Ver_Blank_Time   * ticksPerLine));
    calcTicks.Sync_Start    := integer(round(timing.Ver_Sync_Start   * ticksPerLine));
    calcTicks.Last_Border   := integer(round(timing.V_Bottom_Border  * ticksPerLine));
    calcTicks.Front_Porch   := integer(round(timing.V_Front_Porch    * ticksPerLine));
    calcTicks.Sync_Time     := integer(round(timing.Ver_Sync_Time    * ticksPerLine));
    calcTicks.Back_Porch    := integer(round(timing.V_Back_Porch     * ticksPerLine));
    calcTicks.First_Border  := integer(round(timing.V_Top_Border     * ticksPerLine));
    
    checkSum := calcTicks.Back_Porch +
                calcTicks.First_Border +
                calcTicks.Addr_Time +
                calcTicks.Last_Border +
                calcTicks.Front_Porch +
                calcTicks.Sync_Time;
    if checkSum /= calcTicks.Total_Time then
      report "WARNING: Standard specified vertical total time [" & integer'image(calcTicks.Total_Time) & "]" &
             " differs from sum [" & integer'image(checkSum) & "]! Will use checksum!";
      calcTicks.Total_Time := checkSum;
    end if;
    return calcTicks;
  end function VGA_vsync_ticks;
end package body DMT_VGA_timing;
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.ALL;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.DMT_VGA_timing.all;
------------------------------------------------------------------------------
-- definicija, implementacija h/v sync generatorja za fiksno resolucijo
entity VGA_fixed_timing_sync is
  generic (
    timing              : VGA_timing_ticks := VGA_hsync_ticks(VGA_640x480_60Hz, VGA_640x480_60Hz.Pixel_Clock)
  );
  port (
    clk_i               : in  std_logic; -- ura
    en_i                : in  std_logic; -- enable
    rst_i               : in  std_logic; -- reset
    
    video_active_o      : out std_logic; -- ali smo v "Addresable Video" obmocju
    video_active_pos_o  : out std_logic_vector(getRequiredBits(timing.Addr_Time)-1 downto 0); -- indeks piksla v AV obmocju
    sync_o              : out std_logic; -- h/v sync
    scan_time_over_o    : out std_logic  -- ali smo dosegli total time
  );
end VGA_fixed_timing_sync;

architecture VGA_fixed_timing_sync of VGA_fixed_timing_sync is
  -- vrednost za sync, odvisno od polaritete signala
  constant syncActive   : std_logic := std_logic(to_unsigned(VGA_Sync_Polarity'pos(timing.Sync_Polarity), 1)(0));
  constant syncInactive : std_logic := not syncActive;

  -- stanja ter register stanj avtomata
  type at_state         is (
                            ACTIVE_VIDEO,   -- obmocje uporabne slike (Addressable Video)
                            PRE_SYNC,       -- blanking do sync pulse: obmocje bottom/right border, front porch
                            SYNC,           -- blanking znotraj sync pulse: obmocje sync
                            POST_SYNC,      -- blanking do periode pred koncem cikla: obmocje back porch-1
                            SCAN_TIME_OVER  -- zadnja perioda pred ponovitvijo cikla, prozitev scan_time_over_o
                            );
  signal at_cur_state   : at_state;
  -- za stanje stevca (da ne podvajamo logike za sestevalnike)
  type at_cnt_state     is (
                            CNT_STEP,       -- stevec v tej urini periodi steje
                            CNT_RESET       -- stevec se v tej urini periodi ponastavi
                            );
  signal at_cur_cnt_state          : at_cnt_state;
  -- konstante, da se vsote ne prevedejo v sestevalnike
  -- -1 ker stejemo od 0 ter se -1 ker zaradi implementacije sestevalnika ta registrira reset sele
  -- v naslednji urini periodi
  constant ACTIVE_VIDEO_max_ticks  : integer := timing.Addr_time -1 -1;
  constant PRE_SYNC_max_ticks      : integer := timing.Last_Border + timing.Front_Porch -1 -1;
  constant SYNC_max_ticks          : integer := timing.Sync_time -1 -1;
  -- dodatni -1 ker damo pulz 1 periodo pred koncem (zadnja urina perioda je v SCAN_TIME_OVER)
  constant POST_SYNC_max_ticks     : integer := timing.Back_Porch + timing.First_Border -1 -1 -1;
  
  -- register za stevec avtomata
  -- ta je dovolj velik, da lahko presteje stevilo ciklov najdaljsega casovnega obmocja
  -- (v nasem primeru active video)
  signal at_cnt         : std_logic_vector(getRequiredBits(timing.Addr_Time)-1 downto 0);
  
  -- registri za izhode avtomata (pomnijo izhod ce jih eksplicitno ne spremenimo)
  -- v FPGA je namrec najvecja ovira velikost kombinatorike, ne stevilo uporabljenih registrov
  signal at_video_active,
         at_sync,
         at_scan_time_over
                        : std_logic;
begin
  video_active_o        <= at_video_active;
  video_active_pos_o    <= at_cnt; -- ze enak velikosti naslovljivega dela
  sync_o                <= at_sync;
  scan_time_over_o      <= at_scan_time_over;
  
  -- uporabimo vzorec tvorbe avtomata z dvema procesoma, opisan v XST UG (ver 10.3), str. 248
  
  at_cur_state_next_state: process is
  begin
    wait until rising_edge(clk_i); -- predpostavimo sinhroni reset
    if en_i = '1' or rst_i = '1' then
      if rst_i = '1' then
        at_cur_cnt_state  <= CNT_RESET;
        at_cur_state      <= ACTIVE_VIDEO;
      else
        if at_cur_cnt_state = CNT_STEP then
          at_cnt              <= at_cnt + 1;
        else
          -- iz ponastavitve gremo v stetje
          at_cur_cnt_state    <= CNT_STEP;
          at_cnt              <= (others => '0');
        end if;
      
        case at_cur_state is
          when ACTIVE_VIDEO =>
            if at_cnt = std_logic_vector(to_unsigned(ACTIVE_VIDEO_max_ticks, at_cnt'length)) then
              at_cur_cnt_state  <= CNT_RESET;
              at_cur_state      <= PRE_SYNC;
            end if;
          when PRE_SYNC =>
            if at_cnt = std_logic_vector(to_unsigned(PRE_SYNC_max_ticks, at_cnt'length)) then
              at_cur_cnt_state  <= CNT_RESET;
              at_cur_state      <= SYNC;
            end if;
          when SYNC =>
            if at_cnt = std_logic_vector(to_unsigned(SYNC_max_ticks, at_cnt'length)) then
              at_cur_cnt_state  <= CNT_RESET;
              at_cur_state      <= POST_SYNC;
            end if;
          when POST_SYNC =>
            if at_cnt = std_logic_vector(to_unsigned(POST_SYNC_max_ticks, at_cnt'length)) then
              at_cur_state      <= SCAN_TIME_OVER;
            end if;
          when SCAN_TIME_OVER =>
              at_cur_cnt_state  <= CNT_RESET;
              at_cur_state      <= ACTIVE_VIDEO;
        end case;
      end if;
    end if;
  end process at_cur_state_next_state;
  
  at_output: process is
  begin
    -- cakamo na spremembo stanja
    wait until rising_edge(clk_i);--at_cur_state'event;
    
    if rst_i = '1' then
      -- ponastavimo registre, ki predstavljajo izhod
      at_video_active     <= '0';
      at_sync             <= syncInactive;
      at_scan_time_over   <= '0';
    else
      -- "privzete" vrednosti, razen v primeru, ko jih while posameznega stanja eksplicitno nastavi
      at_video_active     <= '0';
      at_sync             <= syncInactive;
      at_scan_time_over   <= '0';
      
      case at_cur_state is
        when ACTIVE_VIDEO =>
          at_video_active <= '1';
        when PRE_SYNC     =>
          NULL; -- ze zaobjeto s privzetimi vrednostmi
        when SYNC         =>
          at_sync         <= syncActive;
        when POST_SYNC    =>
          NULL;
        when SCAN_TIME_OVER =>
          at_scan_time_over <= '1';
      end case;
    end if;
  end process at_output;
  
end VGA_fixed_timing_sync;
------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.ALL;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.DMT_VGA_timing.all;
------------------------------------------------------------------------------
entity VGA_fixed_timing_ctrl is
  generic (
    timing      : VGA_timing;  --:= VGA_640x480_60Hz;
    base_freq   : real        --:= VGA_640x480_60Hz.Pixel_Clock
  );
  port (
    clk_i                 : in  std_logic;
    rst_i                 : in  std_logic;
    en_i                  : in  std_logic;
    
    h_video_active_o      : out std_logic;
    v_video_active_o      : out std_logic;
    
    h_sync_o              : out std_logic;
    v_sync_o              : out std_logic;
    
    h_video_active_pos_o  : out std_logic_vector(getRequiredBits(timing.Hor_Pixels)-1 downto 0);
    v_video_active_pos_o  : out std_logic_vector(getRequiredBits(timing.Ver_Pixels)-1 downto 0)
  );
end VGA_fixed_timing_ctrl;

architecture VGA_fixed_timing_ctrl of VGA_fixed_timing_ctrl is
  constant h_timing       : VGA_timing_ticks := VGA_hsync_ticks(timing, base_freq);
  constant v_timing       : VGA_timing_ticks := VGA_vsync_ticks(timing, base_freq);

  signal h_scan_time_over : std_logic; -- "rowclk" signal
  
  -- stanja ter register stanj avtomata
  type at_state           is (
                            PRESCALER_STEP,
                            PRESCALER_RESET_IN_SCAN,
                            PRESCALER_RESET_OVER_SCAN
                            );
  signal at_cur_state     : at_state;
  -- prescaler stevec zanj
  -- -1 ker stejemo od 0
  constant PRESCALE_WAIT_max_ticks  : integer := integer(round(base_freq / timing.Pixel_Clock)) -1 -1 -1;
  
  signal at_prescaler_cnt           : std_logic_vector(getRequiredBits(PRESCALE_WAIT_max_ticks) downto 0);
  
  constant h_max_pixels             : integer := Timing.Hor_Pixels-1;
  signal at_h_video_active_pos      : std_logic_vector(getRequiredBits(timing.Hor_Pixels)-1 downto 0);
begin
  h_video_active_pos_o    <= at_h_video_active_pos;
  
  at_cur_state_next_state: process is
  begin
    wait until rising_edge(clk_i);
    if  rst_i = '1' then
        at_cur_state      <= PRESCALER_RESET_OVER_SCAN;
    else
      if at_cur_state = PRESCALER_STEP then
        at_prescaler_cnt  <= at_prescaler_cnt + 1;
      else
        at_prescaler_cnt  <= (others => '0');
        at_cur_state      <= PRESCALER_STEP;
      end if;
      
      if at_prescaler_cnt = std_logic_vector(to_unsigned(PRESCALE_WAIT_max_ticks, at_prescaler_cnt'length)) then
        at_cur_state      <= PRESCALER_RESET_IN_SCAN;
      end if;
      if h_scan_time_over = '1' then
        at_cur_state      <= PRESCALER_RESET_OVER_SCAN;
      end if;
    end if;
  end process at_cur_state_next_state;
  
  at_output: process is
  begin
    wait until rising_edge(clk_i);
    if at_cur_state = PRESCALER_RESET_OVER_SCAN then
      at_h_video_active_pos <= (others => '0');
    else
      -- ob ponastavitvi prescalerja se premaknemo 1 korak s stevcem naprej
      if at_cur_state = PRESCALER_RESET_IN_SCAN then
        at_h_video_active_pos <= at_h_video_active_pos + 1;
      end if;
    end if;
  end process at_output;
  
--  at_cur_state_next_state: process is
--  begin
--    wait until rising_edge(clk_i);
--    if  rst_i = '1' or h_scan_time_over = '1' then
--        pixel_prescaler     <= (others => '0');
--        h_video_active_pos  <= (others => '0');
--    else
--      if pixel_prescaler = std_logic_vector(to_unsigned(pixelPrescalerMax, pixel_prescaler'length)) then
--        pixel_prescaler     <= (others => '0');
--        if h_video_active_pos = hVideoActiveMax then
--          h_video_active_pos  <= (others => '0');
--        else
--          h_video_active_pos  <= h_video_active_pos+1;
--        end if;
--      else
--        pixel_prescaler <= pixel_prescaler + 1;
--      end if;
--    end if;
--  end process at_cur_state_next_state;

  hsync: entity work.VGA_fixed_timing_sync
    generic map (
      timing              => h_timing
    )
    port map (
      clk_i               => clk_i,
      en_i                => en_i,
      rst_i               => rst_i,
      
      video_active_o      => h_video_active_o,
      video_active_pos_o  => open,
      sync_o              => h_sync_o,
      scan_time_over_o    => h_scan_time_over
    );
  vsync: entity work.VGA_fixed_timing_sync
    generic map (
      timing              => v_timing
    )
    port map (
      clk_i               => clk_i,
      en_i                => h_scan_time_over, -- "rowclk" za vsync
      rst_i               => rst_i,
      
      video_active_o      => v_video_active_o,
      video_active_pos_o  => v_video_active_pos_o,
      sync_o              => v_sync_o,
      scan_time_over_o    => open -- XST kljub temu vrne warning
    );
end VGA_fixed_timing_ctrl;

