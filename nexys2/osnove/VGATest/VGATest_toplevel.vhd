----------------------------------------------------------------------------------
-- Create Date:    15:21:34 02/18/2017 
-- Module Name:    VGATest_toplevel
-- Za testiranje VGA timing gen modulov.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.DMT_VGA_timing.all;

entity VGATest_toplevel is
  port (
    clk                     : in  std_logic;
    
    -- za reset
    btn                     : in  std_logic_vector(0 downto 0);
    -- za dcm_locked signal
    led                     : out std_logic_vector(0 downto 0);
    
    vgaRed                  : out std_logic_vector(3 downto 0);
    vgaGreen                : out std_logic_vector(3 downto 0);
    vgaBlue                 : out std_logic_vector(3 downto 0);
    
    Hsync                   : out std_logic;
    Vsync                   : out std_logic
  );
end VGATest_toplevel;

-- za natancna frekvencna obmocja pri Spartan3E FPGA glej DS312, str. 143
-- zaenkrat uporabimo 130 MHz, ki ga generiramo preko CLKFX od DCM

architecture VGATest_toplevel_impl of VGATest_toplevel is
  constant timing           : VGA_timing := VGA_1024x768_60Hz;
  constant base_freq        : real := 130.0; -- Mhz

  -- ura, ki jo generira DCM (in ki jo uporabimo tekom preostalega vezja - pomembno je da je
  -- s tem sintetizirano vezje hitrejse od generirane frekvence!)
  signal dcm_clock          : std_logic;
  signal dcm_locked         : std_logic;

  signal h_video_active     : std_logic;
  signal v_video_active     : std_logic;
  signal h_video_active_pos : std_logic_vector(getRequiredBits(timing.Hor_Pixels)-1 downto 0);
  signal v_video_active_pos : std_logic_vector(getRequiredBits(timing.Ver_Pixels)-1 downto 0);
  
  signal font_data          : std_logic_vector(47 downto 0); -- en simbol ima 48 bitov, 6 v visino, 8 v sirino
  signal font_row           : std_logic_vector(7 downto 0);
  signal font_row_reverse   : std_logic_vector(7 downto 0);
  signal font_address       : std_logic_vector(7 downto 0);
  signal pixel_on           : std_logic;
  
begin
  led(0)  <= dcm_locked;

  vga_dcm : entity work.VGA_DCM
    port map (
      CLKIN_IN              => clk, -- vhodna ura DCM je ura ploscice (50Mhz)
      RST_IN                => btn(0),
      CLKFX_OUT             => dcm_clock,
      CLKIN_IBUFG_OUT       => open,
      CLK0_OUT              => open,
      LOCKED_OUT            => dcm_locked
  );
  
  font_rom : entity work.ROM(ROM_hex)
    generic map ("8x6.rom", 48, 8, false)
    port map (
      clock_in              => dcm_clock,
      data_out              => font_data,
      address_in            => font_address
  );

  vga : entity work.VGA_fixed_timing_ctrl
    generic map (
      timing => timing,
      base_freq => base_freq
    )
    port map (
      clk_i                 => dcm_clock,
      rst_i                 => btn(0),
      en_i                  => '1',
      
      h_video_active_o      => h_video_active,
      v_video_active_o      => v_video_active,
      
      h_sync_o              => Hsync,
      v_sync_o              => Vsync,
      
      h_video_active_pos_o  => h_video_active_pos,
      v_video_active_pos_o  => v_video_active_pos
    );
  
  font_address <= v_video_active_pos(3 downto 3) & h_video_active_pos(9 downto 3);
  row_gen: process is
  begin
    wait until rising_edge(dcm_clock);
		case v_video_active_pos(2 downto 0) is
			when "101" => font_row <= font_data(7 downto 0);
			when "100" => font_row <= font_data(15 downto 8);
			when "011" => font_row <= font_data(23 downto 16);
			when "010" => font_row <= font_data(31 downto 24);
			when "001" => font_row <= font_data(39 downto 32);
			when "000" => font_row <= font_data(47 downto 40);
			when others => font_row <= (others => '0');
		end case;
  end process row_gen;
  
  font_row_reverse(0) <= font_row(7);
  font_row_reverse(1) <= font_row(6);
  font_row_reverse(2) <= font_row(5);
  font_row_reverse(3) <= font_row(4);
  font_row_reverse(4) <= font_row(3);
  font_row_reverse(5) <= font_row(2);
  font_row_reverse(6) <= font_row(1);
  font_row_reverse(7) <= font_row(0);
  
  pattern_gen: process is
  begin
    wait until rising_edge(dcm_clock);
    if h_video_active='1' and v_video_active='1' and font_row_reverse(to_integer(unsigned(h_video_active_pos(2 downto 0)))) = '1' then
      vgaRed    <= (others => '1');
      vgaGreen  <= (others => '1');
      vgaBlue   <= (others => '1');
    else
      vgaRed    <= (others => '0');
      vgaGreen  <= (others => '0');
      vgaBlue   <= (others => '0');
    end if;
  end process pattern_gen;
end VGATest_toplevel_impl;

