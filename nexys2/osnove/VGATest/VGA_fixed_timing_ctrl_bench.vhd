--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:14:12 02/19/2017
-- Design Name:   
-- Module Name:   /home/tpecar/faks/1semester/dn/razvoj/koda/nexys2/osnove/VGATest/VGA_fixed_timing_ctrl_bench.vhd
-- Project Name:  VGATest
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: VGA_fixed_timing_ctrl
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.DMT_VGA_timing.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY VGA_fixed_timing_ctrl_bench IS
END VGA_fixed_timing_ctrl_bench;
 
ARCHITECTURE behavior OF VGA_fixed_timing_ctrl_bench IS 
    
   --Inputs
   signal clk_i : std_logic := '0';
   signal rst_i : std_logic := '0';
   signal en_i : std_logic := '1';

 	--Outputs
   signal h_video_active_o : std_logic;
   signal v_video_active_o : std_logic;
   signal h_sync_o : std_logic;
   signal v_sync_o : std_logic;
   signal h_video_active_pos_o : std_logic_vector(getRequiredBits(VGA_640x480_60Hz.Hor_Pixels)-1 downto 0);
   signal v_video_active_pos_o : std_logic_vector(getRequiredBits(VGA_640x480_60Hz.Ver_Pixels)-1 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: entity work.VGA_fixed_timing_ctrl
      generic map (
        timing    => VGA_640x480_60Hz,
        base_freq => 100.0
      )
      PORT MAP (
        clk_i => clk_i,
        rst_i => rst_i,
        en_i => en_i,
        h_video_active_o => h_video_active_o,
        v_video_active_o => v_video_active_o,
        h_sync_o => h_sync_o,
        v_sync_o => v_sync_o,
        h_video_active_pos_o => h_video_active_pos_o,
        v_video_active_pos_o => v_video_active_pos_o
      );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      rst_i <= '1';
      wait for 100 ns;	
      rst_i <= '0';

      wait for clk_i_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
