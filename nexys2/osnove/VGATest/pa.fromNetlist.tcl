
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name VGATest -dir "/home/tpecar/faks/1semester/dn/razvoj/koda/nexys2/osnove/VGATest/planAhead_run_2" -part xc3s500efg320-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/tpecar/faks/1semester/dn/razvoj/koda/nexys2/osnove/VGATest/VGA_fixed_timing_ctrl.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/tpecar/faks/1semester/dn/razvoj/koda/nexys2/osnove/VGATest} }
set_property target_constrs_file "Nexys2_500General.ucf" [current_fileset -constrset]
add_files [list {Nexys2_500General.ucf}] -fileset [get_property constrset [current_run]]
link_design
