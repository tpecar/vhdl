--------------------------------------------------------------------------------
-- Create Date:   17:18:56 02/05/2017
-- Module Name:   /home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/LEDTest/LEDTest_testbench.vhd
-- Project Name:  LEDTest
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
 
ENTITY LEDTest_testbench IS
END LEDTest_testbench;
 
ARCHITECTURE behavior OF LEDTest_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT LEDTest
    PORT(
         clk : IN   std_ulogic;
         Led : OUT  std_ulogic_vector(7 downto 0);
         sw : IN    std_ulogic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_ulogic := '0';
   signal sw  : std_ulogic_vector(7 downto 0) := B"00000001";

 	--Outputs
   signal Led : std_ulogic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: LEDTest PORT MAP (
          clk => clk,
          Led => Led,
          sw => sw
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
