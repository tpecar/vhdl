----------------------------------------------------------------------------------
-- Create Date:    16:32:15 02/05/2017 
-- Design Name: 
-- Module Name:    LEDTest - LEDTest_implementacija 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- POZOR: NE POZABI dodati UCF datoteke, saj ti bo Xilinx-ovo orodje se vedno
-- z veseljem prevedlo ter tvorilo bitstream datoteko (ki bo vsebovalo to logiko),
-- a ta ne bo vezana na pine FPGA cipa - posledicno absolutno nic ne bo delovalo

-- entity je vmesnik (ang. interface) vezja - je po svoje analog
-- vmesniku, ki ga srecamo v objektno orientiranih jezikih
-- Ta definira
--  - signale, preko katerega je vezje povezano z zunanjim svetom,
--  - generike, ki jih lahko vse implementacije danega vezja uporabljajo
--    (in ki jih lahko tekom instanciranja implementacije tudi spremenimo)
--  - konstante, ki jih lahko vse implementacije danega vezja uporabljajo
--    (in ki jih ni mogoce spreminjati)
--  - pasivne funkcije/procedure/procese/asserte, ki so definirane znotraj
--    begin/end bloka entitete - izvajajo se za vse implementacije in enako
--    kot konstrukti, definirani v implementaciji (architecture bloku),
--    VENDAR s to omejitvijo, da NE smejo vplivati na notranje stanje entitete
--    (lahko le berejo signale) - posledicno se to uporablja za simulacijo
entity LEDTest is
  port (
    -- mapping na pine, definirane znotraj UCF (User Constraint File)
    clk :     std_ulogic; -- ce se ne zapise smer, se predpostavi IN
    Led : out std_ulogic_vector(7 downto 0);
    sw  : in  std_ulogic_vector(7 downto 0)
  );
begin
  -- pasivni konstrukti
  report_test: process
  begin
    wait on clk;
    report "Nekaj lepega" severity note;
  end process report_test;
end LEDTest;

-- architecture je implementacija vmesnika (entity) - definira torej
-- delovanje vezja, ki komunicira z zunanjim svetom preko signalov,
-- definiranih v vmesniku
-- implementacij posameznega vezja je lahko vec - ob instanciranju
-- komponente (torej bloka z specificnim delovanjem, ki se uporablja
-- znotraj neke druge implementacije) moramo povedati, katero
-- implementacijo (arhitekturo) zelimo
--
-- sicer je tudi mozno, da komponenti podamo kar ime entitete za tip,
-- pri cemer se uporabi zadnje prevedena implementacija entitete
-- (to smo poceli pri vajah - ce imas le eno implementacijo, je to
-- recimo da ok, pri cemer niti ni vazno, kaksno je ime implementacije
-- - mi smo recimo uporabljali kar Behavioral)
architecture LEDTest_implementacija of LEDTest is
  -- notranji signali za pine, definirane v UCF
  signal clock_in : std_ulogic;
  signal led_out  : std_ulogic_vector(Led'RANGE);
  signal switch_in: std_ulogic_vector(sw'RANGE);
begin
  -- prevezava pinov na notranje signale
  clock_in  <= clk;
  Led       <= led_out;
  switch_in <= sw;
  
  -- vzporedni proces
  switch_to_led: process
  begin
    -- wait stavek lahko razumemo kot barrier konstrukt pri programiranju
    -- paralelnih sistemov - do njega se lahko vse operacije izvajajo
    -- vzporedno, vse operacije za njim pa se bodo pricele izvajati sele
    -- ko je pogoju, dolocenem za until stavkom, zadoscen
    --
    -- uporaba sensitivity lista pri procesu se prevede v
    -- wait on stavek na koncu procesa, pri cemer je njegov until pogoj
    -- enak ANDu vseh x'EVENT, pri cemer je x signal, specificiran
    -- znotraj sensitivity lista
    --
    -- tu je treba upostevati se sinhronost/asinhronost stavkov
    -- namrec vse kar je za wait blokom, se smatra kot sinhrono glede na
    -- until pogoj
    -- prav tako se v primeru uporabe sensitivity lista ter if stavka vse
    -- kar je v then bloku, smatralo kot sinhrono glede na pogoj tega if stavka.
    --
    -- ce je stavek pred wait (ali tega ni) ter ni del nobenega then bloka, katerega
    -- pogoj je odvisen od spremenljivke v sensitivity listu, potem se dan stavek
    -- smatra kot asinhron - potrebno ga je dodati v until pogoj wait-a oz. v
    -- sensitivity list, ki se implicitno prevede vanj
    wait until rising_edge(clock_in);
    -- daljsi zapis: wait on clock_in until rising_edge(clock_in);
    
    led_out <= switch_in;
  end process switch_to_led;
end architecture LEDTest_implementacija;
