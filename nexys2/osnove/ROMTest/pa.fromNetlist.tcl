
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name ROMTest -dir "/home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/ROMTest/planAhead_run_2" -part xc3s500efg320-5
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/ROMTest/ROMTest_toplevel.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/ROMTest} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "Nexys2_500General.ucf" [current_fileset -constrset]
add_files [list {Nexys2_500General.ucf}] -fileset [get_property constrset [current_run]]
link_design
