----------------------------------------------------------------------------------
-- Create Date:    11:05:56 02/06/2017 
-- Module Name:    ROM - Behavioral 
--
-- ROM, katerega vsebina je instancirana na podlagi datoteke.
-- pri tem se predpostavlja format datoteke
--    - posamezna vrstica datoteke je ena beseda ROM
--      - vsebina vrstice je zapisana sestnajstisko/osmisko/binarno, odvisno od
--        izbrane implementacije
--      - dolzina vsebine doloca velikost besede in mora biti enaka
--        v vseh vrsticah
--    - stevilo vrstic datoteke doloca velikost naslova
--
-- da se uspesno sintetizira, moras prevajalniku podati opcijo
--  -loop_iteration_limit X
-- pri cemer mora X biti najmanj toliko, kolikor je vrstic/znakov v vrstici v
-- ROM datoteki (prvoten limit je namrec 64)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use IEEE.std_logic_textio.all;

library std;
use std.textio.all;
----------------------------------------------------------------------------------

entity ROM is
  generic (file_path : string; data_size : integer; address_size : integer;
           allow_address_wrapping : boolean := false);
  port (
    clock_in    : in  std_logic;
    data_out    : out std_logic_vector(data_size-1 downto 0);
    address_in  : in  std_logic_vector(address_size-1 downto 0)
  );
  --
  -- Razlog, zakaj so spodnje funkcije tako braindead:
  --  - TEXTIO paket nima mehanizma za seekanje datoteke, zato je potrebno
  --    na novo odpreti datoteko vsakic, ko jo zelimo brati od zacetka
  --
  --  - Xilinxov sintetizator nam ne omogoca dereferenciranja kazalcev, posledicno
  --    prides do zabavnih situacij: recimo ko preberes vrstico datoteke, jo
  --    dobis kot kazalec na niz, za pridobitev lastnosti niza pa je le-tega
  --    potrebno dereferencirati - tako da dolocanje dolzine niza postane skoraj
  --    nemogoca operacija
  --
  --    na sreco lahko ta problem obidemo z direktno uporabo funkcij standardne IEEE knjiznice,
  --    ki so zgleda da zmozne interno dereferencirati kazalce, vendar je potrebno
  --    paziti na sledece:
  --      - v primeru read implementacije za branje character-ja ta ne odstrani
  --        interpretiranega dela niza, med tem ko read za string to naredi
  --      - when pogoj exit-a sploh ne testira pogoja (ga vedno evalvira v true), posledicno
  --        je potrebno oviti brezpogojni exit v if stavek z njegovim pogojem
  --      - in naj se slisi se tako bizarno, ampak izgleda da je implementacija read funkcije razlicna
  --        ze med sintezo ter simulacijo - in sicer pri simulaciji se GOOD spremenljivka
  --        obnasa ravno obratno (torej uspesna interpretacija je pri simulaciji signalizirana
  --        kot true, pri sintezi pa false)
  --
  --        ampak ko gledas debug izpis, kaze kot da gre zanka le skozi eno iteracijo, a rezultat, ki ga
  --        izracuna (ki je direktno odvisen od stevila izvedb vsebine zanke) pa je pravilen
  --
  --  - sintetizator ne omogoca uporabe funkcij, ki bi bile zgolj definirane v entiteti,
  --    med tem ko bi se njihova dejanska implmementacija nahajala znotraj arhitekture
  --      - namrec kar se med sintezo zgodi, je, da kljub temu da instanciras
  --        arhitekturo, se entiteta loceno prevede prej, zaradi cesar sintetizator ne najde
  --        funkcij, ki so definirane v arhitekturi - simulator je tega zmozen, ceprav je to
  --        najbrz srecno nakljucje zaradi uporabe C prevajalnika, ki je zmozen vpogleda vnaprej)
  --      - je pa sintetizatorjeva "kratkovidnost" precej selektivna, saj ceprav ni mogoce narediti
  --        prej omenjenega, pa lahko v primeru uporabe paketov ta najde pripadajoca telesa paketov
  --        ter jih prevede, ceprav deklaracijo paketa ter njegove funkcije uporabimo pred
  --        definicijo paketa
  --  - ce obstaja deklaracija in implementacija (brez slednjega niti ni mozno) funkcije znotraj
  --    entitete, potem se pri sintezi ta implementacija uporabi v vseh arhitekturah, ceprav je lahko
  --    znotraj arhitektur implementirana drugace (torej "overridanje" funkcij ni mozno)
  --
  --    Edini mogoca ponovna uporaba kode v Xilinxovem VHDLu je s tem, da lahko znotraj entitete ze podamo
  --    implementacije funkcij, ki se ne sklicujejo na nobeno konstanto/funkcijo, specificno za
  --    posamezno arhitekturo.
  --    V vseh ostalih primerih je potrebno sicer skupne (in enake) funkcije implementirati direktno v
  --    arhitekturah.
  --
  --    Marsikaj od tega bi se lahko veliko elegantneje resilo z genericnimi podprogrami/paketi, ki jih je
  --    uvedel VHDL-2008 standard, a vsaj glede na
  --      https://forums.xilinx.com/t5/Synthesis/How-to-use-generic-packages-as-introduced-in-VHDL-2008/td-p/705147
  --    lahko najbrz zaman upam na podporo znotraj Vivado toolchaina-a, kaj sele pri ISE.. saj ne da je ta
  --    standard bil izdan ~10 let nazaj. Ugh.
  
  -- Ker sintetizator v svoji vesoljni logiki paradoksalno ignorira assert stavke - pri cemer je moznost njihove
  -- uporabe izven simulatorja prisla sele z XST v Vivado toolchainu, vec o tem na:
  --    https://www.xilinx.com/support/answers/65415.html
  -- uporabljamo funkcijo, da ga prisilimo v zaustavitev.
  -- Omejitev tega pristopa je seveda v tem, da mora biti klic vedno znotraj neke funkcije/procesa (assert je lahko
  -- recimo samostojen vzporeden stavek znotraj telesa arhitekture).
  procedure fail is
  begin
    -- ce smo v simulatorju, se uporabi assert
    assert true
      severity FAILURE;
    -- drugace pa naredimo nekaj nesmiselnega, da ustavimo sintetizator
    wait;
  end fail;
  
  -- [ENT] vrne stevilo znakov v vstici
  impure function getLineWidth return integer is
    file romFile : text open read_mode is file_path;
    variable romLine : line;
    variable romLineString : string(1 downto 1);
    variable noChar : boolean := false;
    
    variable lineLength : integer :=0;
  begin
    readline(romFile, romLine);
    loop
      read(romLine, romLineString, noChar); -- beremo po niz dolzine 1, dokler ne zmanjka
      --report "LINELEN "&integer'image(lineLength)&" "&boolean'image(noChar);
      if(
        -- fix za kontradiktorno obnasanje GOOD parametra
        -- pragma translate off
        not
        -- pragma translate on
        noChar) then
        exit; -- ker [exit when noChar] ne dela!
      end if;
      lineLength := lineLength + 1;
    end loop;
    --report "FOUND LENGTH "&integer'image(lineLength);
    return lineLength;
  end function getLineWidth;

  -- [ENT] vrne stevilo vrstic v ROM datoteki
  impure function getNumberOfLines return integer is
    file romFile : text open read_mode is file_path;
    variable romLine : line;
    
    -- za ignoriranje morebitne prazne vrstice na koncu
    -- (v osnovi bi lahko bila tudi kjerkoli znotraj datoteke, a v tem
    -- primeru moramo to upostevati tudi pri dejanskem zapisu vrednosti v array)
    variable romLineChar : character;
    variable lineEmpty : boolean;
    
    variable numLines : integer := 0;
  begin
    while not endfile(romFile) loop
      readline(romFile, romLine);
      read(romLine, romLineChar, lineEmpty);
      --report "LINECNT "&character'image(romLineChar)&" "&boolean'image(lineEmpty);
      if(
      -- pragma translate off
      not
      -- pragma translate on
      lineEmpty) then
        exit;
      end if;
      numLines := numLines + 1;
    end loop;
    --report "FOUND LINES "&integer'image(numLines);
    return numLines;
  end function getNumberOfLines;
  
  -- [ENT] izracuna minimalno potrebno stevilo bitov, ki ga mora vektor hraniti za podan najvecji indeks
  function getAddressSize(maxIndex : integer) return integer is
  begin
    -- ce je maksimalen indeks 0 (1 vrednost), 1 (2 vrednosti), potrebujemo vsaj 1 bit za naslov
    if(maxIndex < 2) then
      return 1;
    end if;
    
    return integer(ceil(log2(real(maxIndex))));
  end function getAddressSize;
begin
end ROM;

architecture ROM_hex of ROM is
  ------ deklaracije sintetizabilnih funkcij, inicializacija konstant --------
  -- [ENT] getLineWidth
  -- [ENT] getNumberOfLines
  constant lineWidth        : integer := getLineWidth;
  constant numberOfLines    : integer := getNumberOfLines;
  -- [ENT] getAddressSize
  constant romAddressSize   : integer := getAddressSize(numberOfLines-1); -- -1 ker gre za maksimalni indeks, ti pa se zacnejo z 0
  function getDataSize(lineWidth : integer) return integer;               -- za izracun stevila bitov
  constant romDataSize      : integer := getDataSize(lineWidth);
  subtype tyROMLine is std_logic_vector(romDataSize-1 downto 0);
  -- ROM podatkovni tip - pri tem ima ta VEDNO stevilo elementov zaokrozeno navzgor na naslednjo potenco 2
  -- sintetizator namrec interno predvideva, da bo v primeru array-a ta __vedno__ imel toliko elementov, kolikor je moznih
  -- vrednosti za indeks pri podanem stevilu bitov za vektor indeksa, pa ce je to glede na kontekst programa mozno ali ne
  -- (torej tudi v primeru omejitve vrednosti ta ne bo uposteval, da ni mogoce doseci neveljavnega indeksa)
  --    - to je tudi izvor "Index value(s) does not match array range, simulation mismatch." napak
  --    - prav tako v primeru te napake sintetizator predpostavi za vrednost, ki bi jo moral pridobiti iz array-a, kar
  --      kar vrednost 0, s cimer se ROM optimizira ven
  -- zgornje (neuporabljene) vrednosti pustimo privzeto inicializirane (torej 0)
  type tyROMArray is array ((2**romAddressSize)-1 downto 0) of tyROMLine;
  impure function initROM return tyROMArray;                              -- za tvorbo ROMa
  ----------------------------------------------------------------------------
  ---------------- deklaracija funkcij za namen simulacije -------------------
  -- pragma translate off
  function dataToLine(dataLineVector : std_logic_vector) return string;
  procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  ----------- implementacija sintetizabilnih funkcij, konstant ---------------
  -- [ARCH] dolocitev stevila bitov v posamezni vrstici ROMa
  function getDataSize(lineWidth : integer) return integer is
  begin
    return lineWidth * 4;
  end function getDataSize;
  
  -- [ARCH] inicializacija ROMa
  -- ce bi zeleli inicializirati z bitnim poljem, lahko uporabimo
  --  variable bit_vector_romLine : bit_vector(tyROMLine'range);
  -- ...
  -- begin
  -- ...
  --  readline(romFile, romLine);
  --  read(romLine, bit_vector_romLine);
  --  romDataIn(curLine) := to_stdlogicvector(bit_vector_romLine);
  --
  -- za hex/oct pa lahko kar direktno uporabimo std_logic_textio knjiznico
  --
  impure function initROM return tyROMArray is
    file romFile : text open read_mode is file_path;
    variable romLine : line;
    
    variable romDataIn : tyROMArray;
  begin
    for curLine in 0 to numberOfLines-1 loop
      readline(romFile, romLine);
      hread(romLine, romDataIn(curLine));
    end loop;
    return romDataIn;
  end function initROM;
  
  -- konstanta, ki nosi vsebino ROMa
  -- pri tem PAZI: v primeru da se v funkciji, ki inicializira konstanto, pojavi napaka,
  -- se pri sintezi/simulaciji konstanta potiho inicializira na privzeto vrednost
  constant romData : tyROMArray := initROM;
  
  ----------------------------------------------------------------------------
  ---------------- implementacija funkcij za namen simulacije ----------------
  -- pragma translate off
  -- [ARCH] za pretvorbo podatkovne vrstice ROMa v niz, za namen izpisa
  function dataToLine(dataLineVector : std_logic_vector) return string is
    variable dataLine : line;
  begin
    hwrite(dataLine, dataLineVector, LEFT, 0);
    -- line je kazalec na string tip, .all ga dereferencira
    -- to je mozno narediti le v simulatorju
    return dataLine.all;
  end function dataToLine;
  
  -- [ENT] za izpis informacije o generiranem ROMu --
  procedure testROM is
    constant curLineHexBits : integer := integer(ceil(real(romAddressSize)/4.0)*4.0);
    variable curLineHex     : line;
  begin
    report "ROM bits per line : " & integer'image(romDataSize);
    report "ROM lines         : " & integer'image(numberOfLines) & " (" & integer'image(romAddressSize) & " address bits)";
    report "---------------- ROM data ----------------";
    for curLine in 0 to tyROMArray'high loop
      hwrite(curLineHex, std_logic_vector(to_unsigned(curLine, curLineHexBits)), LEFT, 0);
      report curLineHex.all & ": " & dataToLine(romData(curLine));
      
      -- ponastavimo spremenljivki (dealociramo trenutno vsebino nizov)
      deallocate(curLineHex);
    end loop;
    report "------------ end of ROM data -------------";
  end procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
begin
  ------------------------------ simulacija ----------------------------------
  -- pragma translate off
  sim_testROM: process is
  begin
    testROM;
    wait; -- izvedemo le enkrat
  end process sim_testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  -- proces za "assert" stavke
  sanity_check: process is
  begin
    -- v primeru, ko je naslovno vodilo vecje od dejanskega naslova, sprozimo napako
    -- (razen ce z generikom to eksplicitno dovolimo)
    if (address_in'high > romAddressSize and not allow_address_wrapping) then
      report "ERROR: address_in bus width exceeds actual ROM address width " &
             "[address_in : " & integer'image(address_in'high) & " > romAddressSize : " & integer'image(romAddressSize) & "]";
      report "to explicitly allow this and use only lower address bits, set allow_address_wrapping => true. Will fail now.";
      fail;
    end if;
    wait until rising_edge(clock_in); -- da utisamo warning za pomanjkanje ure, ta del kode se namrec ne sintetizira
  end process sanity_check;

  process is
  begin
    wait until rising_edge(clock_in);
    data_out(tyROMLine'range) <= romData(to_integer(unsigned(address_in(romAddressSize-1 downto 0))))(tyROMLine'range);
  end process;
end ROM_hex;

architecture ROM_oct of ROM is
  ------ deklaracije sintetizabilnih funkcij, inicializacija konstant --------
  -- [ENT] getLineWidth
  -- [ENT] getNumberOfLines
  constant lineWidth        : integer := getLineWidth;
  constant numberOfLines    : integer := getNumberOfLines;
  -- [ENT] getAddressSize
  constant romAddressSize   : integer := getAddressSize(numberOfLines-1);
  function getDataSize(lineWidth : integer) return integer;
  constant romDataSize      : integer := getDataSize(lineWidth);
  subtype tyROMLine is std_logic_vector(romDataSize-1 downto 0);
  type tyROMArray is array ((2**romAddressSize)-1 downto 0) of tyROMLine;
  impure function initROM return tyROMArray;
  ----------------------------------------------------------------------------
  ---------------- deklaracija funkcij za namen simulacije -------------------
  -- pragma translate off
  function dataToLine(dataLineVector : std_logic_vector) return string;
  procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  ----------- implementacija sintetizabilnih funkcij, konstant ---------------
  -- [ARCH] dolocitev stevila bitov v posamezni vrstici ROMa
  function getDataSize(lineWidth : integer) return integer is
  begin
    return lineWidth * 3;
  end function getDataSize;
  
  -- [ARCH] inicializacija ROMa
  impure function initROM return tyROMArray is
    file romFile : text open read_mode is file_path;
    variable romLine : line;
    
    variable romDataIn : tyROMArray;
  begin
    for curLine in 0 to numberOfLines-1 loop
      readline(romFile, romLine);
      oread(romLine, romDataIn(curLine));
    end loop;
    return romDataIn;
  end function initROM;
  
  -- konstanta, ki nosi vsebino ROMa
  constant romData : tyROMArray := initROM;
  
  ----------------------------------------------------------------------------
  ---------------- implementacija funkcij za namen simulacije ----------------
  -- pragma translate off
  -- [ARCH] za pretvorbo podatkovne vrstice ROMa v niz, za namen izpisa
  function dataToLine(dataLineVector : std_logic_vector) return string is
    variable dataLine : line;
  begin
    owrite(dataLine, dataLineVector, LEFT, 0);
    return dataLine.all;
  end function dataToLine;
  
  -- [ENT] za izpis informacije o generiranem ROMu --
  procedure testROM is
    constant curLineHexBits : integer := integer(ceil(real(romAddressSize)/4.0)*4.0);
    variable curLineHex     : line;
  begin
    report "ROM bits per line : " & integer'image(romDataSize);
    report "ROM lines         : " & integer'image(numberOfLines) & " (" & integer'image(romAddressSize) & " address bits)";
    report "---------------- ROM data ----------------";
    for curLine in 0 to tyROMArray'high loop
      hwrite(curLineHex, std_logic_vector(to_unsigned(curLine, curLineHexBits)), LEFT, 0);
      report curLineHex.all & ": " & dataToLine(romData(curLine));
      deallocate(curLineHex);
    end loop;
    report "------------ end of ROM data -------------";
  end procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
begin
  ------------------------------ simulacija ----------------------------------
  -- pragma translate off
  sim_testROM: process is
  begin
    testROM;
    wait;
  end process sim_testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  -- proces za "assert" stavke
  sanity_check: process is
  begin
    if (address_in'high > romAddressSize and not allow_address_wrapping) then
      report "ERROR: address_in bus width exceeds actual ROM address width " &
             "[address_in : " & integer'image(address_in'high) & " > romAddressSize : " & integer'image(romAddressSize) & "]";
      report "to explicitly allow this and use only lower address bits, set allow_address_wrapping => true. Will fail now.";
      fail;
    end if;
    wait until rising_edge(clock_in);
  end process sanity_check;

  process is
  begin
    wait until rising_edge(clock_in);
    data_out(tyROMLine'range) <= romData(to_integer(unsigned(address_in(romAddressSize-1 downto 0))))(tyROMLine'range);
  end process;
end ROM_oct;

architecture ROM_bin of ROM is
  ------ deklaracije sintetizabilnih funkcij, inicializacija konstant --------
  -- [ENT] getLineWidth
  -- [ENT] getNumberOfLines
  constant lineWidth        : integer := getLineWidth;
  constant numberOfLines    : integer := getNumberOfLines;
  -- [ENT] getAddressSize
  constant romAddressSize   : integer := getAddressSize(numberOfLines-1);
  function getDataSize(lineWidth : integer) return integer;
  constant romDataSize      : integer := getDataSize(lineWidth);
  subtype tyROMLine is std_logic_vector(romDataSize-1 downto 0);
  type tyROMArray is array ((2**romAddressSize)-1 downto 0) of tyROMLine;
  impure function initROM return tyROMArray;
  ----------------------------------------------------------------------------
  ---------------- deklaracija funkcij za namen simulacije -------------------
  -- pragma translate off
  function dataToLine(dataLineVector : std_logic_vector) return string;
  procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  ----------- implementacija sintetizabilnih funkcij, konstant ---------------
  -- [ARCH] dolocitev stevila bitov v posamezni vrstici ROMa
  function getDataSize(lineWidth : integer) return integer is
  begin
    return lineWidth;
  end function getDataSize;
  
  -- [ARCH] inicializacija ROMa
  impure function initROM return tyROMArray is
    file romFile : text open read_mode is file_path;
    variable romLine : line;
    variable romLineBits : bit_vector(romDataSize-1 downto 0);
    
    variable romDataIn : tyROMArray;
  begin
    for curLine in 0 to numberOfLines-1 loop
      readline(romFile, romLine);
      read(romLine, romLineBits);
      romDataIn(curLine) := to_stdlogicvector(romLineBits);
    end loop;
    return romDataIn;
  end function initROM;
  
  -- konstanta, ki nosi vsebino ROMa
  constant romData : tyROMArray := initROM;
  
  ----------------------------------------------------------------------------
  ---------------- implementacija funkcij za namen simulacije ----------------
  -- pragma translate off
  -- [ARCH] za pretvorbo podatkovne vrstice ROMa v niz, za namen izpisa
  function dataToLine(dataLineVector : std_logic_vector) return string is
    variable dataLine : line;
  begin
    write(dataLine, dataLineVector);
    return dataLine.all;
  end function dataToLine;
  
  -- [ENT] za izpis informacije o generiranem ROMu --
  procedure testROM is
    constant curLineHexBits : integer := integer(ceil(real(romAddressSize)/4.0)*4.0);
    variable curLineHex     : line;
  begin
    report "ROM bits per line : " & integer'image(romDataSize);
    report "ROM lines         : " & integer'image(numberOfLines) & " (" & integer'image(romAddressSize) & " address bits)";
    report "---------------- ROM data ----------------";
    for curLine in 0 to tyROMArray'high loop
      hwrite(curLineHex, std_logic_vector(to_unsigned(curLine, curLineHexBits)), LEFT, 0);
      report curLineHex.all & ": " & dataToLine(romData(curLine));
      deallocate(curLineHex);
    end loop;
    report "------------ end of ROM data -------------";
  end procedure testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
begin
  ------------------------------ simulacija ----------------------------------
  -- pragma translate off
  sim_testROM: process is
  begin
    testROM;
    wait;
  end process sim_testROM;
  -- pragma translate on
  ----------------------------------------------------------------------------
  -- proces za "assert" stavke
  sanity_check: process is
  begin
    if (address_in'high > romAddressSize and not allow_address_wrapping) then
      report "ERROR: address_in bus width exceeds actual ROM address width " &
             "[address_in : " & integer'image(address_in'high) & " > romAddressSize : " & integer'image(romAddressSize) & "]";
      report "to explicitly allow this and use only lower address bits, set allow_address_wrapping => true. Will fail now.";
      fail;
    end if;
    wait until rising_edge(clock_in);
  end process sanity_check;

  process is
  begin
    wait until rising_edge(clock_in);
    data_out(tyROMLine'range) <= romData(to_integer(unsigned(address_in(romAddressSize-1 downto 0))))(tyROMLine'range);
  end process;
end ROM_bin;