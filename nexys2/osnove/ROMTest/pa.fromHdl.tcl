
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name ROMTest -dir "/home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/ROMTest/planAhead_run_1" -part xc3s500efg320-5
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "Nexys2_500General.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {ROM.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {ROMTest_toplevel.vhd}]]
set_property file_type VHDL $hdlfile
set_property library work $hdlfile
set_property top ROMTest_toplevel $srcset
add_files [list {Nexys2_500General.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc3s500efg320-5
