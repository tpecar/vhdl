--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:36:06 02/06/2017
-- Design Name:   
-- Module Name:   /home/tpecar/faks/1semester/dn/razvoj/nexys2/osnove/ROMTest/ROM_testbench.vhd
-- Project Name:  ROMTest
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ROM
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ROM_testbench IS
  port(
    clk         : in  std_logic;
    hex_data_out: out std_logic_vector(47 downto 0);
    oct_data_out: out std_logic_vector(47 downto 0);
    bin_data_out: out std_logic_vector(47 downto 0);
    address_in  : in  std_logic_vector(7 downto 0)
  ); -- potreben vsaj clk, ce zelis da se topmodul prevede
END ROM_testbench;
 
ARCHITECTURE behavior OF ROM_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
  component ROM is
    generic (file_path : string; data_size : integer; address_size : integer);
    port (
      clock_in    : in  std_logic;
      data_out    : out std_logic_vector(47 downto 0);
      address_in  : in  std_logic_vector(7 downto 0)
    );
  end component ROM;
    
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
   signal clock : std_logic;
   constant clock_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   myHexRom: entity work.ROM(ROM_hex)
    generic map (file_path => "8x6.rom", data_size => 48, address_size => 8)
    port map (
      clock_in => clk,
      data_out => hex_data_out,
      address_in => address_in
    );
   myOctRom: entity work.ROM(ROM_oct)
    generic map (file_path => "oct_rom_testfile.rom", data_size => 48, address_size => 8, allow_address_wrapping=>true)
    port map (
      clock_in => clk,
      data_out => oct_data_out,
      address_in => address_in
    );
   myBinRom: entity work.ROM(ROM_bin)
    generic map (file_path => "rom_testfile.rom", data_size => 48, address_size => 8, allow_address_wrapping=>true)
    port map (
      clock_in => clk,
      data_out => bin_data_out,
      address_in => address_in
    );

   -- Clock process definitions
   clock_process :process
   begin
		clock <= '0';
		wait for clock_period/2;
		clock <= '1';
		wait for clock_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clock_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
