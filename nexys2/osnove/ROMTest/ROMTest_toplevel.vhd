----------------------------------------------------------------------------------
-- Create Date:    11:00:10 02/06/2017 
-- Design Name: 
-- Module Name:    ROMTest_toplevel - ROMTest_toplevel_impl 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- tu bomo le tvorili ROM modul ter ga simulirali, ne bomo ga dejansko testirali na
-- FPGA

entity ROMTest_toplevel is
  port(
    clk     : in  std_logic;
    Led     : out std_logic_vector(7 downto 0);
    sw      : in  std_logic_vector(7 downto 0)
  ); -- potreben vsaj clk, ce zelis da se topmodul prevede
begin
end ROMTest_toplevel;

architecture ROMTest_toplevel_impl of ROMTest_toplevel is
  
  -- dummy signal
  signal hex_data_out: std_logic_vector(47 downto 0);
  signal oct_data_out: std_logic_vector(47 downto 0);
  signal bin_data_out: std_logic_vector(47 downto 0);
  signal address_in: std_logic_vector(7 downto 0);
  
  component ROM is
    generic (file_path : string; data_size : integer; address_size : integer);
    port (
      clock_in    : in  std_logic;
      data_out    : out std_logic_vector(data_size-1 downto 0);
      address_in  : in  std_logic_vector(address_size-1 downto 0)
    );
  end component ROM;
begin
   myHexRom: entity work.ROM(ROM_hex)
    generic map (file_path => "8x6.rom", data_size => 48, address_size => 8)
    port map (
      clock_in => clk,
      data_out => hex_data_out,
      address_in => address_in
    );
   myOctRom: entity work.ROM(ROM_oct)
    generic map (file_path => "oct_rom_testfile.rom", data_size => 48, address_size => 8, allow_address_wrapping=>true)
    port map (
      clock_in => clk,
      data_out => oct_data_out,
      address_in => address_in
    );
   myBinRom: entity work.ROM(ROM_bin)
    generic map (file_path => "rom_testfile.rom", data_size => 48, address_size => 8, allow_address_wrapping=>true)
    port map (
      clock_in => clk,
      data_out => bin_data_out,
      address_in => address_in
    );
   Led <= hex_data_out(7 downto 0) xor oct_data_out(7 downto 0) xor bin_data_out(7 downto 0); -- prikazujemo le spodnjih 8 bitov
   
   process is
   begin
    wait until rising_edge(clk);
    address_in <= sw;
   end process;
end ROMTest_toplevel_impl;

--configuration conf of ROMTest_toplevel is
--  for ROMTest_toplevel_impl
--    for all : ROM use entity work.ROM(ROM_hex);
--    end for;
--  end for;
--end configuration conf;